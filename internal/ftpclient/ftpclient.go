/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package ftpclient

import (
	"fmt"
	"io/ioutil"
	"net"
	"time"

	"github.com/pkg/sftp"
	"gitlab.com/team-escrow/escrow-rde-client/internal/config"
	"golang.org/x/crypto/ssh"
)

type SFtpClient struct {
	SftpClient *sftp.Client
	SshClient  *ssh.Client
}

func NewFtpClient(ftpconfig *config.SshConfig) (*SFtpClient, error) {
	var hostkey ssh.PublicKey
	addr := fmt.Sprintf("%s:%d", ftpconfig.SshHostname, ftpconfig.SshPort)
	key, err := ioutil.ReadFile(ftpconfig.SshPrivateKeyPath)
	if err != nil {
		return nil, fmt.Errorf("unable to read private key: %v", err)
	}

	var signer ssh.Signer
	// Create the Signer for this private key.
	if len(ftpconfig.SshPrivateKeyPassword) > 0 {

		signer, err = ssh.ParsePrivateKeyWithPassphrase(key, []byte(ftpconfig.SshPrivateKeyPassword))
		if err != nil {
			return nil, fmt.Errorf("unable to parse private key: %v", err)
		}

	} else {
		signer, err = ssh.ParsePrivateKey(key)
		if err != nil {
			return nil, fmt.Errorf("unable to parse private key: %v", err)
		}
	}

	sshconfig := new(ssh.ClientConfig)
	sshconfig.User = ftpconfig.SshUsername
	sshconfig.Auth = []ssh.AuthMethod{ssh.PublicKeys(signer)}

	// read the trusted hostkey from config
	if len(ftpconfig.SshHostPublicKeyPath) > 0 {
		hostkeybytes, err := ioutil.ReadFile(ftpconfig.SshHostPublicKeyPath)
		if err != nil {
			return nil, fmt.Errorf("unable to read the hostkey from file %s: %s", ftpconfig.SshHostPublicKeyPath, err.Error())
		}
		hostkey, _, _, _, err = ssh.ParseAuthorizedKey(hostkeybytes)
		if err != nil {
			return nil, fmt.Errorf("unable to parse the hostkey %s: %s", ftpconfig.SshHostPublicKeyPath, err.Error())
		}
		sshconfig.HostKeyCallback = ssh.FixedHostKey(hostkey)

	} else {
		sshconfig.HostKeyCallback = func(hostname string, remote net.Addr, key ssh.PublicKey) error {
			fmt.Printf("Remote presents this key: %#v\n", key)
			fmt.Printf("key: %#s\n", string(key.Marshal()))
			return nil
		}
	}
	sshconfig.Timeout = time.Duration(10 * time.Second)

	// Connect to the remote server and perform the SSH handshake.
	conn, err := ssh.Dial("tcp", addr, sshconfig)
	if err != nil {
		return nil, fmt.Errorf("Failed to dial: " + err.Error())
	}
	client, err := sftp.NewClient(conn)
	if err != nil {
		return nil, fmt.Errorf("Failed to create client: " + err.Error())
	}

	ftpclient := new(SFtpClient)
	ftpclient.SftpClient = client
	ftpclient.SshClient = conn
	return ftpclient, nil
}
