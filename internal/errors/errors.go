/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package errors

/* ###################### */
/* ### Datastructures ### */
/* ###################### */

// MultipleEntryError occurs when a record with same key (first field) already exists.
type MultipleEntryError struct {
	Msg string
}

func (e MultipleEntryError) Error() string { return e.Msg }

// EntryNotFoundError occurs when retrieving a non existing value from a database.
type EntryNotFoundError struct {
	Msg string
}

func (e EntryNotFoundError) Error() string { return e.Msg }

// InvalidColumnCountError occurs on adding a new row with invalid column count to a database.
type InvalidColumnCountError struct {
	Msg string
}

func (e InvalidColumnCountError) Error() string { return e.Msg }

// InvalidDatabaseEntry occurs when an invalid entry is found in the database.
type InvalidDatabaseEntry struct {
	Msg string
}

func (e InvalidDatabaseEntry) Error() string { return e.Msg }

/* ################ */
/* ### Deposits ### */
/* ################ */

// CsvTooLargeError occurs when a csv file is too large according to ICANN.
type CsvTooLargeError struct {
	Msg string
}

func (e CsvTooLargeError) Error() string { return e.Msg }

// TooManyRowsError occurs when a csv file contains too many rows according to ICANN.
type TooManyRowsError struct {
	Msg string
}

func (e TooManyRowsError) Error() string { return e.Msg }

// ColumnNotFoundError occurs when querying a non existing column from a deposit.
type ColumnNotFoundError struct {
	Msg string
}

func (e ColumnNotFoundError) Error() string { return e.Msg }

// MultipleDepositFilesError occurs when multiple full, incremental or handle files are found in single deposit mode.
type MultipleDepositFilesError struct {
	Msg string
}

func (e MultipleDepositFilesError) Error() string { return e.Msg }

// FullAndIncrementalDepositError occurs when both full and incremental files are found in the same deposit.
type FullAndIncrementalDepositError struct {
	Msg string
}

func (e FullAndIncrementalDepositError) Error() string { return e.Msg }

// NeitherFullNorIncrementalDepositError occurs when neither full, nor incremental files are found in a deposit.
type NeitherFullNorIncrementalDepositError struct {
	Msg string
}

func (e NeitherFullNorIncrementalDepositError) Error() string { return e.Msg }

// MissingSequenceNumberError occurs when a multiple file from a deposit is not indexed by a sequence number.
type MissingSequenceNumberError struct {
	Msg string
}

func (e MissingSequenceNumberError) Error() string { return e.Msg }

// InvalidSequenceNumberError occurs when a deposit file with invalid sequence number is found.
type InvalidSequenceNumberError struct {
	Msg string
}

func (e InvalidSequenceNumberError) Error() string { return e.Msg }

// ConflictingSequenceNumberError occurs when multiple files from a deposit share the same sequence number.
type ConflictingSequenceNumberError struct {
	Msg string
}

func (e ConflictingSequenceNumberError) Error() string { return e.Msg }

// MissingSequenceFile occurs when a file sequence is incomplete.
type MissingSequenceFile struct {
	Msg string
}

func (e MissingSequenceFile) Error() string { return e.Msg }

// EmptyDepositFileError occurs when the first file of a deposit sequence is empty and does not contain headers.
type EmptyDepositFileError struct {
	Msg string
}

func (e EmptyDepositFileError) Error() string { return e.Msg }

/* ################## */
/* ### Validation ### */
/* ################## */

// ValidateReadCsvFileError occurs when the file cannot be opened for reading
type ValidateReadCsvFileError struct {
	Msg string
}

func (e ValidateReadCsvFileError) Error() string { return e.Msg }

// ValidateParseCsvFileError occurs when the parsing of the cvs files fails because of fault
// formats according to RFC4180
type ValidateParseCsvFileError struct {
	Msg string
}

func (e ValidateParseCsvFileError) Error() string { return e.Msg }

// ValidateMultipleEntryError occurs when a record with same key (first field) already exists.
type ValidateMultipleEntryError struct {
	Msg string
}

func (e ValidateMultipleEntryError) Error() string { return e.Msg }

// ValidateMissingHandleFileError occurs when a deposit uses handles but does not include a handle file.
type ValidateMissingHandleFileError struct {
	Msg string
}

func (e ValidateMissingHandleFileError) Error() string { return e.Msg }

// ValidateNameserverFieldError occurs when the nameserver fields of a domain records are missing.
type ValidateNameserverFieldError struct {
	Msg string
}

func (e ValidateNameserverFieldError) Error() string { return e.Msg }

// ValidateHandleNotFoundError occurs when the handle stated in the domain record
// cannot be found in the handle file.
type ValidateHandleNotFoundError struct {
	Msg string
}

func (e ValidateHandleNotFoundError) Error() string { return e.Msg }

// ValidateInvalidExpiryDateFormatError occurs when the expiration date of the
// domain cannot be parsed.
type ValidateInvalidExpiryDateFormatError struct {
	Msg string
}

func (e ValidateInvalidExpiryDateFormatError) Error() string { return e.Msg }

// ValidateEmptyHandleIdentifierError occurs when the first column of a handle file line is empty and does not contain a handle identifier.
type ValidateEmptyHandleIdentifierError struct {
	Msg string
}

func (e ValidateEmptyHandleIdentifierError) Error() string { return e.Msg }

// ValidateWrongHandleColumnNameError occurs when first column of handle file is not titled "handle" or "hdl".
type ValidateWrongHandleColumnNameError struct {
	Msg string
}

func (e ValidateWrongHandleColumnNameError) Error() string { return e.Msg }

// ValidateInvalidHeaderFormatError occurs when a header field contains forbidden chars
type ValidateInvalidHeaderFormatError struct {
	Msg string
}

func (e ValidateInvalidHeaderFormatError) Error() string { return e.Msg }

// ValidateMissingHeaderFieldError occurs when a mandatory header field is missing
type ValidateMissingHeaderFieldError struct {
	Msg string
}

func (e ValidateMissingHeaderFieldError) Error() string { return e.Msg }

// ValidateNoValidDomainNameError occurs when a domainname is not a valid hostname.
type ValidateNoValidDomainNameError struct {
	Msg string
}

func (e ValidateNoValidDomainNameError) Error() string { return e.Msg }

/* ###################### */
/* ### Crypto Errors  ### */
/* ###################### */

// ReadKeyError occurs when a private or public key could not be read from the source.
type ReadKeyError struct {
	Msg string
}

func (e ReadKeyError) Error() string { return e.Msg }

// DecryptKeyError occurs when a protected key could not be decrypted.
type DecryptKeyError struct {
	Msg string
}

func (e DecryptKeyError) Error() string { return e.Msg }

// MissingSignatureError occurs when an unsigned file is read by the descryption tool.
type MissingSignatureError struct {
	Msg string
}

func (e MissingSignatureError) Error() string { return e.Msg }

// WrongSignatureError occurs when the signature does not verify the content of a message.
type WrongSignatureError struct {
	Msg string
}

func (e WrongSignatureError) Error() string { return e.Msg }
