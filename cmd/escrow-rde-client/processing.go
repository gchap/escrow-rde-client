/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"crypto/sha256"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"time"

	"gitlab.com/team-escrow/escrow-rde-client/internal/escryptlib"
	"gitlab.com/team-escrow/escrow-rde-client/internal/ftpclient"
)

var (
	archiveFileList []string
)

func compressAndEncrypt(files []os.FileInfo) error {
	_, err := os.Stat(appconfig.RunDir)
	if os.IsNotExist(err) {
		err := os.Mkdir(appconfig.RunDir, 0755)
		if err != nil {
			log.Fatalf("ERROR: Cannot create run directory %s: %s", appconfig.RunDir, err.Error())
		}
	}

	// create a working dir
	t := time.Now()
	currentProcessingDir := fmt.Sprintf("%d-%02d-%02d-%02d%02d%02d",
		t.Year(),
		t.Month(),
		t.Day(),
		t.Hour(),
		t.Minute(),
		t.Second())
	workdir = appconfig.RunDir + "/" + currentProcessingDir
	os.Mkdir(workdir, 0755)
	log.Printf("directory for this run: %s", workdir)
	log.Printf("sha256 of receivers public key file: %s", getRecvPubkeyFileHash())

	basefilestamp := fmt.Sprintf("%d-%02d-%02d", t.Year(), t.Month(), t.Day())
	basefilename := appconfig.IanaID + "_RDE_" + basefilestamp + "_"
	hashfile := workdir + "/" + basefilename + "hash.txt"

	for _, csvfile := range files {
		dontModifyFilename, err := regexp.MatchString("\\d{1,4}_RDE_\\d{4}-\\d{2}-\\d{2}_.*(full|inc|hdl).*", csvfile.Name())
		if err != nil {
			return fmt.Errorf("failed parsing original filename: %s", err.Error())
		}

		if dontModifyFilename {
			log.Printf("filename %s is already in target format", csvfile.Name())
			basefilename = ""
		}

		srcFileName := appconfig.DepositBaseDir + "/" + csvfile.Name()

		// copy the basefile to workdir with correct naming.
		workingFileBaseName := basefilename + csvfile.Name()
		workingFileName := workdir + "/" + basefilename + csvfile.Name()
		log.Printf(" processing file %s as %s", filepath.Base(srcFileName), workingFileBaseName)

		log.Printf("  copy %s to %s", csvfile.Name(), workingFileName)
		if err := copyFileToWorkDirAndRename(srcFileName, workingFileName); err != nil {
			return fmt.Errorf("copy failed: %s", err.Error())
		}

		log.Printf("  store sha256 hash of %s in file %s", workingFileBaseName, hashfile)

		err = storeHashOfFile(workingFileName, hashfile)
		if err != nil {
			return fmt.Errorf("store sha256 hash failed: %s", err.Error())
		}
		if err := gzipAndEncryptFile(workingFileName); err != nil {
			return fmt.Errorf("gzip and encryption failed: %s", err.Error())
		}
		// remove workingFile
		err = os.Remove(workingFileName)
		if err != nil {
			return err
		}

	}
	archiveFileList = append(archiveFileList, hashfile)
	return nil
}

func copyFileToWorkDirAndRename(srcfilename, dstfilename string) error {
	out, err := os.OpenFile(dstfilename, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	defer out.Close()

	f, err := os.Open(srcfilename)
	if err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	defer f.Close()

	if _, err = io.Copy(out, f); err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	out.Sync()

	return nil
}

func storeHashOfFile(filename string, hashfilename string) error {
	hasher := sha256.New()
	f, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	defer f.Close()

	_, err = io.Copy(hasher, f)
	if err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	hashfile, err := os.OpenFile(hashfilename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return fmt.Errorf("%s", err.Error())
	}
	defer hashfile.Close()

	hashline := fmt.Sprintf("%x %s\n", hasher.Sum(nil), filepath.Base(filename))
	hashfile.WriteString(hashline)
	hashfile.Close()

	return nil
}

func gzipAndEncryptFile(srcfile string) error {
	dstfile := srcfile + ".gz.gpg"
	outfile, err := os.Create(dstfile)
	if err != nil {
		return err
	}
	// no defer here. file needs to be closed asap to write the encrypted data to it
	outfile.Close()

	if err := escryptlib.EscryptFile(srcfile, dstfile, appconfig.GpgConfig.GpgPrivateKeyPass, appconfig.GpgConfig.GpgPrivateKeyPath, appconfig.GpgConfig.GpgReceiverPubKeyPath); err != nil {
		return fmt.Errorf("failed to generate and write compressed/decrypted content: %s", err.Error())
	}

	log.Printf("  wrote gpg file: %s", filepath.Base(outfile.Name()))
	archiveFileList = append(archiveFileList, outfile.Name())
	return nil
}

func sftpUpload() error {
	client, err := ftpclient.NewFtpClient(appconfig.SshConfig)
	if err != nil {
		return fmt.Errorf("failed to create sftp connection: %s", err.Error())
	}
	// Close connection
	defer client.SftpClient.Close()
	defer client.SshClient.Close()
	log.Printf(" connection established successfully")
	cwd, err := client.SftpClient.Getwd()
	log.Printf(" remote working directory: %s:%s", appconfig.SshConfig.SshHostname, cwd)

	for _, f := range archiveFileList {
		remotefile, err := client.SftpClient.Create(filepath.Base(f))
		defer remotefile.Close()
		if err != nil {
			return err
		}
		localfile, err := os.Open(f)
		defer localfile.Close()
		if err != nil {
			return err
		}
		log.Printf(" uploading: %s", localfile.Name())
		_, err = io.Copy(remotefile, localfile)
		if err != nil {
			return fmt.Errorf(" %s: %s", localfile.Name(), err.Error())
		}
	}
	return nil
}

func getRecvPubkeyFileHash() string {
	f, err := os.Open(appconfig.GpgConfig.GpgReceiverPubKeyPath)
	if err != nil {
		log.Fatalf("ERROR: Cannot read %s", appconfig.GpgConfig.GpgReceiverPubKeyPath)
	}
	defer f.Close()

	h := sha256.New()
	io.Copy(h, f)
	return fmt.Sprintf("%x", h.Sum(nil))
}
