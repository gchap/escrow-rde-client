/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"gitlab.com/team-escrow/escrow-rde-client/internal/config"
	"gitlab.com/team-escrow/escrow-rde-client/internal/deposits"
	"gitlab.com/team-escrow/escrow-rde-client/internal/logging"
	"gitlab.com/team-escrow/escrow-rde-client/internal/util"
	"gitlab.com/team-escrow/escrow-rde-client/internal/validation"

	wf "github.com/danielb42/whiteflag"
	"github.com/sebidude/configparser"
)

const appname = "escrow-rde-client"

var (
	buildtime  string
	gitcommit  string
	appversion string

	printversion = false
	initconfig   = false
	silent       = false

	configfile = "config.yaml"

	workdir string

	appconfig config.AppConfig
)

func cleanup() {
	if r := recover(); r != nil {
		fmt.Println()
		fmt.Println("###################")
		fmt.Printf("Fatal error: %v\n", r)
	}
}

func main() {
	defer cleanup()

	readCommandLine()

	if initconfig {
		createConfigFile()
		os.Exit(0)
	}

	if printversion {
		fmt.Printf("Builddate: %s\n", buildtime)
		fmt.Printf("Version  : %s\n", appversion)
		fmt.Printf("Revision : %s\n", gitcommit)
		fmt.Println(licenseNotice)
		os.Exit(0)
	}

	// parse the config
	err := configparser.ParseYaml(configfile, &appconfig)
	if err != nil {
		log.Fatalf("error parsing config: %v", err.Error())
	}

	log.SetFlags(0)
	logger := new(logging.Logger)

	if len(appconfig.LogFile) > 0 {
		logfile, err := os.OpenFile(appconfig.LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0660)
		if err != nil {
			log.Fatalf("error opening logfile: %v", err.Error())
		}
		defer logfile.Close()
		logger.Destination = logfile

	} else {
		appconfig.LogFile = "stdout"
		logger.Destination = os.Stdout
	}

	cwd, err := os.Getwd()
	if err != nil {
		log.Fatalln(err.Error())
	}

	log.SetOutput(logger)
	log.Printf("=== %s ===", appname)
	log.Printf("working directory     : %s", cwd)
	log.Printf("using configfile      : %s", configfile)
	log.Printf("ianaID                : %s", appconfig.IanaID)
	log.Printf("depositBaseDir        : %s", appconfig.DepositBaseDir)
	log.Printf("runDir                : %s", appconfig.RunDir)
	log.Printf("compressAndEncrypt    : %t", appconfig.CompressAndEncrypt)
	log.Printf("uploadFiles           : %t", appconfig.UploadFiles)
	log.Printf("multi                 : %t", appconfig.Multi)
	log.Printf("useFileSystemCache    : %t", appconfig.UseFileSystemCache)
	log.Printf("gpgPrivateKeyPath     : %s", appconfig.GpgConfig.GpgPrivateKeyPath)
	log.Printf("gpgPrivateKeyPass     : %s", "(not displayed)")
	log.Printf("gpgReceiverPubKeyPath : %s", appconfig.GpgConfig.GpgReceiverPubKeyPath)
	log.Printf("sshHostname           : %s", appconfig.SshConfig.SshHostname)
	log.Printf("sshPort               : %d", appconfig.SshConfig.SshPort)
	log.Printf("sshUsername           : %s", appconfig.SshConfig.SshUsername)
	log.Printf("sshPrivateKeyPath     : %s", appconfig.SshConfig.SshPrivateKeyPath)
	log.Printf("sshPrivateKeyPassword : %s", "(not displayed)")
	log.Printf("sshHostPublicKeyPath  : %s", appconfig.SshConfig.SshHostPublicKeyPath)

	// check if the basedir really exists.
	_, err = os.Stat(appconfig.DepositBaseDir)
	if os.IsNotExist(err) {
		log.Fatalf("desposit directory %s does not exist: %s", appconfig.DepositBaseDir, err.Error())
	}

	watcher := util.NewMemoryWatcher(time.Second)
	watcher.Start()

	di, err := deposits.NewDepositInfo(appconfig.DepositBaseDir, appconfig.Multi)
	if err != nil {
		log.Fatalln(err.Error())
	}

	if !appconfig.SkipValidation {
		reporter, err := makeReporter()
		if err != nil {
			log.Fatalln(err.Error())
		}
		defer reporter.Close()

		options := validation.NewOptions(appconfig.UseFileSystemCache)
		if err := validation.ValidateDeposit(di, reporter, options); err != nil {
			log.Fatalln("Technical error during validation: " + err.Error())
		}

		printReporter(reporter)
	}
	validationMemoryConsumption := watcher.FormatPeakMemoryUsage()

	runtime.GC()

	if appconfig.CompressAndEncrypt {
		log.Println("compress encrypt and sign deposit files")
		err := compressAndEncrypt(di.GetDepositFiles())
		if err != nil {
			log.Fatalln(err.Error())
		}
	}

	runtime.GC()

	if appconfig.CompressAndEncrypt && appconfig.UploadFiles {
		log.Printf("sftp upload to: %s:%d", appconfig.SshConfig.SshHostname, appconfig.SshConfig.SshPort)
		err = sftpUpload()
		if err != nil {
			log.Fatalf("fail to upload files to ftp: %s", err.Error())
		}
	}

	period, _ := watcher.Stop()
	log.Println("Peak Memory Usage:      ", watcher.FormatPeakMemoryUsage())
	log.Println("Validation Memory Usage:", validationMemoryConsumption)
	log.Println("Process Duration:       ", period)
}

func readCommandLine() {
	wf.Alias("i", "init", "Create an example configfile. If the file already exists it will be overwritten.")
	wf.Alias("c", "config", "Location of config file")
	wf.Alias("v", "version", "Print version and licensing information and exit")
	wf.Alias("s", "silent", "Do not print validation results on the command line")

	wf.ParseCommandLine()

	initconfig = wf.CheckBool("init")
	silent = wf.CheckBool("silent")
	printversion = wf.CheckBool("version")

	if wf.CheckString("config") {
		configfile = wf.GetString("config")
	}
}

func makeReporter() (validation.Reporter, error) {
	if silent {
		return validation.NewSilentReporter(), nil
	}
	if appconfig.UseFileSystemCache {
		return validation.NewFileSystemReporter()
	}
	return validation.NewMemoryReporter(), nil
}

func printReporter(reporter validation.Reporter) {
	log.Printf("Validation result: %v infos, %v errors\n", reporter.GetInfoCount(), reporter.GetErrorCount())
	if r, ok := reporter.(*validation.SilentReporter); ok {
		if reporter.HasErrors() {
			log.Fatalln("Last error: " + r.GetLastError().Error())
		}
	} else if r, ok := reporter.(validation.HistoryReporter); ok {
		err := r.WriteToLog()
		if err != nil {
			log.Println("Unable to output deposit validation result: " + err.Error())
		}
	}

	if reporter.HasErrors() {
		log.Fatalln("VALIDATION FAILED")
	} else {
		log.Println("VALIDATION SUCCESSFUL")
	}
}
