/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package deposits

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

var (
	validDomainHeader = "domain,expiry_date,nameserver,nameserver2,rt-handle,ac-handle,tc-handle,bc-handle"
	validDomainLine   = "test.abc,2038-06-19 13:00:00,ns1.bla.foo,ns2.bla.foo,ABC-123,ABC-123,ABC-123,ABC-123"
	validDomainLine2  = "test2.abc,2038-06-19 13:00:00,ns1.bla.foo,ns2.bla.foo,ABC-123,ABC-123,ABC-123,ABC-123"

	validHandleHeader = "handle,name,address,email,phone,fax"
	validHandleLine   = "ABC-123,Mr. Letter Alphabet,ABC Street 123,abc@abc.abc,+1.312.1234532,"
)

func TestSingleFullDeposit(t *testing.T) {
	di := mustMakeDeposit(arr(validDomainHeader, validDomainLine), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	assert.Equal(t, true, di.IsFullDeposit())
	assert.Equal(t, false, di.IsIncrementalDeposit())
	assert.Equal(t, 2, len(di.GetDepositFiles()))
	assert.Equal(t, 1, len(di.GetFullCsvFiles()))
	assert.Equal(t, 0, len(di.GetIncrementalCsvFiles()))
	assert.Equal(t, 1, len(di.GetHandleCsvFiles()))
	assert.Equal(t, true, di.HasDomainFiles())
	assert.Equal(t, true, di.HasHandleFiles())
	assert.Equal(t, true, di.RequiresHandles())
	assert.Equal(t, strings.Split(validDomainHeader, ","), di.GetDomainHeaders())
	assert.Equal(t, strings.Split(validHandleHeader, ","), di.GetHandleHeaders())
}

func TestSingleIncrementalDeposit(t *testing.T) {
	di := mustMakeDeposit(nil, arr(validDomainHeader, validDomainLine), arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	assert.Equal(t, false, di.IsFullDeposit())
	assert.Equal(t, true, di.IsIncrementalDeposit())
	assert.Equal(t, 2, len(di.GetDepositFiles()))
	assert.Equal(t, 0, len(di.GetFullCsvFiles()))
	assert.Equal(t, 1, len(di.GetIncrementalCsvFiles()))
	assert.Equal(t, 1, len(di.GetHandleCsvFiles()))
	assert.Equal(t, true, di.HasDomainFiles())
	assert.Equal(t, true, di.HasHandleFiles())
	assert.Equal(t, true, di.RequiresHandles())
	assert.Equal(t, strings.Split(validDomainHeader, ","), di.GetDomainHeaders())
	assert.Equal(t, strings.Split(validHandleHeader, ","), di.GetHandleHeaders())
}

func TestSingleHandleOnlyDeposit(t *testing.T) {
	dir := prepareDeposit(nil, nil, arr(validHandleHeader, validHandleLine))
	_, err := NewDepositInfo(dir, false)
	assertError(t, errors.NeitherFullNorIncrementalDepositError{}, err)
}

func TestSingleConflictingDeposit(t *testing.T) {
	_, err := makeDeposit(arr(validDomainHeader, validDomainLine), arr(validDomainHeader, validDomainLine), arr(validHandleHeader, validHandleLine))
	assertError(t, errors.FullAndIncrementalDepositError{}, err)
}

func TestMultiFullDeposit(t *testing.T) {
	di := mustMakeMultiDeposit(arr(validDomainHeader, validDomainLine, validDomainLine2), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	assert.Equal(t, true, di.IsFullDeposit())
	assert.Equal(t, false, di.IsIncrementalDeposit())
	assert.Equal(t, 3, len(di.GetDepositFiles()))
	assert.Equal(t, 2, len(di.GetFullCsvFiles()))
	assert.Equal(t, 0, len(di.GetIncrementalCsvFiles()))
	assert.Equal(t, 1, len(di.GetHandleCsvFiles()))
	assert.Equal(t, true, di.HasDomainFiles())
	assert.Equal(t, true, di.HasHandleFiles())
	assert.Equal(t, true, di.RequiresHandles())
	assert.Equal(t, strings.Split(validDomainHeader, ","), di.GetDomainHeaders())
	assert.Equal(t, strings.Split(validHandleHeader, ","), di.GetHandleHeaders())
}

func TestMultiIncrementalDeposit(t *testing.T) {
	di := mustMakeMultiDeposit(nil, arr(validDomainHeader, validDomainLine, validDomainLine2), arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)
	assert.Equal(t, false, di.IsFullDeposit())
	assert.Equal(t, true, di.IsIncrementalDeposit())
	assert.Equal(t, 3, len(di.GetDepositFiles()))
	assert.Equal(t, 0, len(di.GetFullCsvFiles()))
	assert.Equal(t, 2, len(di.GetIncrementalCsvFiles()))
	assert.Equal(t, 1, len(di.GetHandleCsvFiles()))
	assert.Equal(t, true, di.HasDomainFiles())
	assert.Equal(t, true, di.HasHandleFiles())
	assert.Equal(t, true, di.RequiresHandles())
	assert.Equal(t, strings.Split(validDomainHeader, ","), di.GetDomainHeaders())
	assert.Equal(t, strings.Split(validHandleHeader, ","), di.GetHandleHeaders())
}

func TestMultiHandleFilesDeposit(t *testing.T) {
	di := mustMakeMultiDeposit(arr(validDomainHeader, validDomainLine, validDomainLine2), nil, arr(validHandleHeader, validHandleLine, validDomainLine2))
	defer deleteDeposit(di)
	assert.Equal(t, true, di.IsFullDeposit())
	assert.Equal(t, false, di.IsIncrementalDeposit())
	assert.Equal(t, 4, len(di.GetDepositFiles()))
	assert.Equal(t, 2, len(di.GetFullCsvFiles()))
	assert.Equal(t, 0, len(di.GetIncrementalCsvFiles()))
	assert.Equal(t, 2, len(di.GetHandleCsvFiles()))
	assert.Equal(t, true, di.HasDomainFiles())
	assert.Equal(t, true, di.HasHandleFiles())
	assert.Equal(t, true, di.RequiresHandles())
	assert.Equal(t, strings.Split(validDomainHeader, ","), di.GetDomainHeaders())
	assert.Equal(t, strings.Split(validHandleHeader, ","), di.GetHandleHeaders())
}

func TestMultiHandleOnlyDeposit(t *testing.T) {
	dir := prepareMultiDeposit(nil, nil, arr(validHandleHeader, validHandleLine, validDomainLine2))
	_, err := NewDepositInfo(dir, true)
	assertError(t, errors.NeitherFullNorIncrementalDepositError{}, err)
}

func TestMultiConflictingDeposit(t *testing.T) {
	_, err := makeMultiDeposit(arr(validDomainHeader, validDomainLine), arr(validDomainHeader, validDomainLine), arr(validHandleHeader, validHandleLine))
	assertError(t, errors.FullAndIncrementalDepositError{}, err)
}

func TestMissingSequenceNumber(t *testing.T) {
	dir := prepareMultiDeposit(arr(validDomainHeader, validDomainLine, validDomainLine2), nil, arr(validHandleHeader, validHandleLine))
	os.Rename(path.Join(dir, "full_2.csv"), path.Join(dir, "full.csv"))
	_, err := NewDepositInfo(dir, true)
	assertError(t, errors.MissingSequenceNumberError{}, err)
}

func TestConflictingSequenceNumber(t *testing.T) {
	dir := prepareMultiDeposit(arr(validDomainHeader, validDomainLine, validDomainLine2), nil, arr(validHandleHeader, validHandleLine))
	os.Rename(path.Join(dir, "full_2.csv"), path.Join(dir, "data_full_1.csv"))
	_, err := NewDepositInfo(dir, true)
	assertError(t, errors.ConflictingSequenceNumberError{}, err)
}

func TestMissingSequenceFile(t *testing.T) {
	dir := prepareMultiDeposit(arr(validDomainHeader, validDomainLine, validDomainLine2), nil, arr(validHandleHeader, validHandleLine))
	os.Rename(path.Join(dir, "full_2.csv"), path.Join(dir, "full_3.csv"))
	_, err := NewDepositInfo(dir, true)
	assertError(t, errors.MissingSequenceFile{}, err)
}

func TestFindDomainColumnIndex(t *testing.T) {
	di := mustMakeDeposit(arr(validDomainHeader, validDomainLine), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)

	index, err := di.FindDomainColumnIndex("rt-handle")
	assertError(t, nil, err)
	assert.Equal(t, 4, index)

	_, err = di.FindDomainColumnIndex("notexisting")
	assertError(t, errors.ColumnNotFoundError{}, err)
}

func TestFindHandleColumnIndex(t *testing.T) {
	di := mustMakeDeposit(arr(validDomainHeader, validDomainLine), nil, arr(validHandleHeader, validHandleLine))
	defer deleteDeposit(di)

	index, err := di.FindHandleColumnIndex("email")
	assertError(t, nil, err)
	assert.Equal(t, 3, index)

	_, err = di.FindHandleColumnIndex("notexisting")
	assertError(t, errors.ColumnNotFoundError{}, err)
}

/* ##################### */
/* #### test helper #### */
/* ##################### */

func writeLines(file string, lines []string) {
	if lines != nil {
		var sb strings.Builder
		for _, line := range lines {
			sb.WriteString(line)
			sb.WriteRune('\n')
		}
		if err := ioutil.WriteFile(file, []byte(sb.String()), 0644); err != nil {
			panic(err)
		}
	}
}
func prepareDeposit(fullLines, incLines, hdlLines []string) string {
	dir, err := ioutil.TempDir("", "pkg-deposits-test")
	if err != nil {
		panic(err)
	}
	writeLines(path.Join(dir, "full.csv"), fullLines)
	writeLines(path.Join(dir, "inc.csv"), incLines)
	writeLines(path.Join(dir, "hdl.csv"), hdlLines)
	return dir
}
func makeDeposit(fullLines, incLines, hdlLines []string) (*DepositInfo, error) {
	dir := prepareDeposit(fullLines, incLines, hdlLines)
	di, err := NewDepositInfo(prepareDeposit(fullLines, incLines, hdlLines), false)
	if err != nil {
		os.RemoveAll(dir)
		return nil, err
	}
	return di, nil
}
func mustMakeDeposit(fullLines, incLines, hdlLines []string) *DepositInfo {
	di, err := makeDeposit(fullLines, incLines, hdlLines)
	if err != nil {
		panic(err)
	}
	return di
}

func writeMultiLines(prefix, suffix string, lines []string) {
	if lines != nil {
		var sb strings.Builder
		fileNum := 1
		for i, line := range lines {
			sb.WriteString(line)
			sb.WriteRune('\n')
			if i > 0 || len(lines) == 1 {
				// every line should be placed in its own file, except for the first two lines (header+content)
				if err := ioutil.WriteFile(prefix+strconv.Itoa(fileNum)+suffix, []byte(sb.String()), 0644); err != nil {
					panic(err)
				}
				fileNum++
				sb.Reset()
			}
		}
	}
}
func prepareMultiDeposit(fullLines, incLines, hdlLines []string) string {
	dir, err := ioutil.TempDir("", "pkg-deposits-test")
	if err != nil {
		panic(err)
	}
	writeMultiLines(path.Join(dir, "full_"), ".csv", fullLines)
	writeMultiLines(path.Join(dir, "inc_"), ".csv", incLines)
	writeMultiLines(path.Join(dir, "hdl_"), ".csv", hdlLines)
	return dir
}
func makeMultiDeposit(fullLines, incLines, hdlLines []string) (*DepositInfo, error) {
	dir := prepareMultiDeposit(fullLines, incLines, hdlLines)
	di, err := NewDepositInfo(dir, true)
	if err != nil {
		os.RemoveAll(dir)
		return nil, err
	}
	return di, nil
}
func mustMakeMultiDeposit(fullLines, incLines, hdlLines []string) *DepositInfo {
	di, err := makeMultiDeposit(fullLines, incLines, hdlLines)
	if err != nil {
		panic(err)
	}
	return di
}

func deleteDeposit(di *DepositInfo) {
	os.RemoveAll(di.GetBaseDir())
}
func arr(lines ...string) []string {
	array := make([]string, len(lines))
	for i, line := range lines {
		array[i] = line
	}
	return array
}

func assertError(t *testing.T, expected error, actual error) bool {
	if fmt.Sprintf("%T", expected) != fmt.Sprintf("%T", actual) {
		assert.Fail(t, fmt.Sprintf("Errors not equal:\n  Expected: %T\n  Actual:   %T", expected, actual))
		return false
	}
	return true
}
