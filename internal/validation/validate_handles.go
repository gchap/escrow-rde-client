/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package validation

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/team-escrow/escrow-rde-client/internal/datastructures"

	"gitlab.com/team-escrow/escrow-rde-client/internal/deposits"
	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

var (
	// MandatoryHandleKeys state the keys which must be stated in the handle deposit.
	MandatoryHandleKeys = []string{"name", "address", "email", "phone", "fax"}
)

func loadAndCheckHandles(di *deposits.DepositInfo, reporter Reporter, options Options) (*datastructures.CsvCache, error) {
	checkHandleHeaders(di, reporter)

	validator, err := newHandleCsvValidator(di, reporter)
	if err != nil {
		return nil, err
	}

	processor, err := newCsvProcessor(di.GetHandleHeaders(), validator, options)
	if err != nil {
		processor.GetCache().Close()
		return nil, err
	}

	if err := processor.Validate(di.GetHandleCsvFiles(), reporter); err != nil {
		processor.GetCache().Close()
		return nil, err
	}

	return processor.GetCache(), nil
}

type handleCsvValidator struct {
	reporter Reporter
}

func (validator *handleCsvValidator) ReportFileBegin(filePath string) {
	log.Printf("Process handle file '%v'\n", filePath)
}
func (validator *handleCsvValidator) ValidateRow(values []string) error {
	if len(values[0]) == 0 {
		putError(validator.reporter,
			errors.ValidateEmptyHandleIdentifierError{
				Msg: fmt.Sprint("First column of handle row must not be empty."),
			})
	}

	return nil
}
func (validator *handleCsvValidator) ReportDuplicateEntry(values []string) {
	putError(validator.reporter, errors.ValidateMultipleEntryError{
		Msg: fmt.Sprintf("Handle '%v' is listed multiple times", values[0])})
}

func newHandleCsvValidator(di *deposits.DepositInfo, reporter Reporter) (*handleCsvValidator, error) {
	return &handleCsvValidator{reporter}, nil
}

func checkHandleHeaders(di *deposits.DepositInfo, reporter Reporter) {
	// first col must contain handle or "unambigious abbrevation" (4.1.14)
	handleheaders := di.GetHandleHeaders()
	if handleheaders[0] != "handle" && handleheaders[0] != "hdl" {
		putError(reporter,
			errors.ValidateWrongHandleColumnNameError{
				Msg: fmt.Sprint("First column of handle file must be titled 'handle' or 'hdl'."),
			})
	}

	for _, checkkey := range MandatoryHandleKeys {
		found := false
		for _, handlekey := range handleheaders {
			if strings.Contains(strings.Replace(strings.ToLower(handlekey), "-", "", -1), checkkey) {
				found = true
			}
		}
		if !found {
			putError(reporter,
				errors.ValidateMissingHeaderFieldError{
					Msg: fmt.Sprintf("Missing required field prefixed with '%s'.", checkkey),
				})
		}
	}
}
