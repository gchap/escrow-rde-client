/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package datastructures

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

func TestFileSystemCsvCache(t *testing.T) {
	cache, err := NewCsvCache([]string{"key", "col", "col2"}, true)
	if err != nil {
		panic(err)
	}
	defer cache.Close()

	testCsvCache(t, cache)
}

func TestMemoryCsvCache(t *testing.T) {
	cache, err := NewCsvCache([]string{"key", "col", "col2"}, false)
	if err != nil {
		panic(err)
	}
	defer cache.Close()

	testCsvCache(t, cache)
}

func testCsvCache(t *testing.T, cache *CsvCache) {
	line1 := []string{"foo", "bar", "data"}
	line2 := []string{"foo", "bar2", "updated"}
	line3 := []string{"bar", "foo"}
	line4 := []string{"value test", "content", "content 2"}
	line5 := []string{"value test 2", "column1", "column 2"}
	assert.Equal(t, 0, cache.GetCount())
	assert.Equal(t, nil, cache.Add(line1))
	assertCacheGet(t, cache, line1)
	assert.Equal(t, 1, cache.GetCount())
	assertError(t, errors.MultipleEntryError{}, cache.Add(line2))
	assertCacheGet(t, cache, line1)
	assert.Equal(t, 1, cache.GetCount())
	assertCacheContains(t, cache, "not", false)
	assertCacheContains(t, cache, "foo", true)
	_, err := cache.Get("not")
	assertError(t, errors.EntryNotFoundError{}, err)
	assertError(t, errors.InvalidColumnCountError{}, cache.Add(line3))
	assert.Equal(t, 1, cache.GetCount())
	assert.Equal(t, nil, cache.Add(line4))
	assert.Equal(t, 2, cache.GetCount())
	assert.Equal(t, nil, cache.Add(line5))
	assert.Equal(t, 3, cache.GetCount())
}

func assertCacheGet(t *testing.T, cache *CsvCache, expected []string) {
	result, err := cache.Get(expected[0])
	assert.Equal(t, nil, err)
	assert.Equal(t, expected, result)
}

func assertCacheContains(t *testing.T, cache *CsvCache, key string, expected bool) {
	result, err := cache.ContainsKey(key)
	assert.Equal(t, nil, err)
	assert.Equal(t, expected, result)
}
