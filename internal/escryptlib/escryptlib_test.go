/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package escryptlib

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

func TestEscryptDescrypt(t *testing.T) {
	testEscryptDescrypt(t, "test/test_input_ascii.txt")
	testEscryptDescrypt(t, "test/test_input_binary.bin")
}
func testEscryptDescrypt(t *testing.T, inputFile string) {
	tmpEncryptedFile, err := ioutil.TempFile("", "deposit-tools-escryptlib-test")
	if err != nil {
		panic(err)
	}
	defer os.Remove(tmpEncryptedFile.Name())
	err = EscryptFile(inputFile, tmpEncryptedFile.Name(), "test", "test/private.key", "test/public.key")
	assertError(t, nil, err)

	tmpDecryptedFile, err := ioutil.TempFile("", "deposit-tools-escryptlib-test")
	if err != nil {
		panic(err)
	}
	defer os.Remove(tmpDecryptedFile.Name())
	err = DescryptFile(tmpEncryptedFile.Name(), tmpDecryptedFile.Name(), "test", "test/private.key", "test/public.key")
	assertError(t, nil, err)

	originalData, err := ioutil.ReadFile(inputFile)
	if err != nil {
		panic(err)
	}
	decryptData, err := ioutil.ReadFile(tmpDecryptedFile.Name())
	if err != nil {
		panic(err)
	}

	assert.Equal(t, originalData, decryptData)
}

func TestReadPrivateKeyError(t *testing.T) {
	tmpEncryptedFile, err := ioutil.TempFile("", "deposit-tools-escryptlib-test")
	if err != nil {
		panic(err)
	}
	defer os.Remove(tmpEncryptedFile.Name())
	err = EscryptFile("test/test_input_ascii.txt", tmpEncryptedFile.Name(), "test", "test/private.notexistent.key", "test/public.key")
	assertError(t, errors.ReadKeyError{}, err)
}

func TestReadPublicKeyError(t *testing.T) {
	tmpEncryptedFile, err := ioutil.TempFile("", "deposit-tools-escryptlib-test")
	if err != nil {
		panic(err)
	}
	defer os.Remove(tmpEncryptedFile.Name())
	err = EscryptFile("test/test_input_ascii.txt", tmpEncryptedFile.Name(), "test", "test/private.key", "test/public.notexistent.key")
	assertError(t, errors.ReadKeyError{}, err)
}

func TestReadInvalidKeyError(t *testing.T) {
	tmpEncryptedFile, err := ioutil.TempFile("", "deposit-tools-escryptlib-test")
	if err != nil {
		panic(err)
	}
	defer os.Remove(tmpEncryptedFile.Name())
	err = EscryptFile("test/test_input_ascii.txt", tmpEncryptedFile.Name(), "test", "test/invalid.key", "test/public.key")
	assertError(t, errors.ReadKeyError{}, err)
}

func TestAbortOnUnsignedFile(t *testing.T) {
	tmpDecryptedFile, err := ioutil.TempFile("", "deposit-tools-escryptlib-test")
	if err != nil {
		panic(err)
	}
	defer os.Remove(tmpDecryptedFile.Name())
	err = DescryptFile("test/test_input_unsigned.gpg", tmpDecryptedFile.Name(), "test", "test/private.key", "test/public.key")
	assertError(t, errors.MissingSignatureError{}, err)
}

func TestAbortOnWrongSenderPubkey(t *testing.T) {
	tmpDecryptedFile, err := ioutil.TempFile("", "deposit-tools-escryptlib-test")
	if err != nil {
		panic(err)
	}
	defer os.Remove(tmpDecryptedFile.Name())
	err = DescryptFile("test/test_output.gpg", tmpDecryptedFile.Name(), "test", "test/private.key", "test/public.wrong.key")
	assertError(t, errors.WrongSignatureError{}, err)
}

func TestAbortOnWrongPassphrase(t *testing.T) {
	tmpEncryptedFile, err := ioutil.TempFile("", "deposit-tools-escryptlib-test")
	if err != nil {
		panic(err)
	}
	defer os.Remove(tmpEncryptedFile.Name())
	err = EscryptFile("test/test_input_ascii.txt", tmpEncryptedFile.Name(), "TOTALLYWRONGPASSPHRASE", "test/private.key", "test/public.key")
	assertError(t, errors.DecryptKeyError{}, err)
}

/* ##################### */
/* #### test helper #### */
/* ##################### */

func assertError(t *testing.T, expected error, actual error) bool {
	if fmt.Sprintf("%T", expected) != fmt.Sprintf("%T", actual) {
		assert.Fail(t, fmt.Sprintf("Errors not equal:\n  Expected: %T\n  Actual:   %T", expected, actual))
		return false
	}
	return true
}
