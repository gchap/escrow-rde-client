/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package escryptlib

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
	"golang.org/x/crypto/openpgp"
)

func decryptPrivateKey(entity *openpgp.Entity, passphrase string) error {

	if entity.PrivateKey != nil && entity.PrivateKey.Encrypted {
		err := entity.PrivateKey.Decrypt([]byte(passphrase))
		if err != nil {
			return errors.DecryptKeyError{Msg: "Failed to decrypt private key"}
		}
	}

	for _, subkey := range entity.Subkeys {
		if subkey.PrivateKey != nil && subkey.PrivateKey.Encrypted {
			err := subkey.PrivateKey.Decrypt([]byte(passphrase))
			if err != nil {
				return errors.DecryptKeyError{Msg: "Failed to decrypt a subkey"}
			}
		}
	}

	return nil

}

func checkSignature(md *openpgp.MessageDetails, signerPubkey []byte) error {

	pubkey, err := openpgp.ReadArmoredKeyRing(bytes.NewBuffer(signerPubkey))
	if err != nil {
		return errors.ReadKeyError{Msg: "Failed to read signing key"}
	}

	if !md.IsSigned || md.SignedBy == nil {
		return errors.MissingSignatureError{Msg: "No signature or error in signature"}
	}

	if md.SignedBy.PublicKey.Fingerprint != pubkey[0].PrimaryKey.Fingerprint {
		return errors.WrongSignatureError{Msg: "Wrong signature"}
	}

	return nil

}

func importKey(keyData []byte) (openpgp.EntityList, error) {
	// armored keyrings (ascii encoded) begin with -----BEGIN PGP MESSAGE-----
	keyBytes := bytes.NewBuffer(keyData)
	var key openpgp.EntityList
	var err error
	if keyData[0] == 45 && keyData[1] == 45 && keyData[2] == 45 {
		// 45 is the ascii code of '-'
		key, err = openpgp.ReadArmoredKeyRing(keyBytes)
	} else {
		// no --- found at the beginning -> probably default binary key
		key, err = openpgp.ReadKeyRing(keyBytes)
	}

	if err != nil {
		return nil, errors.ReadKeyError{Msg: fmt.Sprintf("Unable to read key: %s", err)}
	}
	return key, nil
}

func importProtectedKey(keyData []byte, passphrase string) (openpgp.EntityList, error) {
	entityList, err := importKey(keyData)
	if err != nil {
		return nil, err
	}

	err = decryptPrivateKey(entityList[0], passphrase)
	if err != nil {
		return nil, err
	}

	return entityList, nil
}

func importUnprotectedKey(keyData []byte) (openpgp.EntityList, error) {
	return importKey(keyData)
}

// EscryptFile performs gzip compression and gpg encryption and signing to the given file.
func EscryptFile(srcFile string, dstFile string, passphrase string, privateKeyFile, publicKeyFile string) error {
	privateKeyData, err := ioutil.ReadFile(privateKeyFile)
	if err != nil {
		return errors.ReadKeyError{Msg: fmt.Sprintf("Failed to read the private key: %s", err.Error())}
	}
	publicKeyData, err := ioutil.ReadFile(publicKeyFile)
	if err != nil {
		return errors.ReadKeyError{Msg: fmt.Sprintf("Failed to read the public key: %s", err.Error())}
	}

	src, err := os.Open(srcFile)
	if err != nil {
		return err
	}
	defer src.Close()

	dst, err := os.OpenFile(dstFile, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer dst.Close()

	return escrypt(src, dst, passphrase, privateKeyData, publicKeyData)
}

func escrypt(src io.Reader, dst io.Writer, passphrase string, privateKeyData []byte, publicKeyData []byte) error {
	privateKey, err := importProtectedKey(privateKeyData, passphrase)
	if err != nil {
		return err
	}
	publicKey, err := importUnprotectedKey(publicKeyData)
	if err != nil {
		return err
	}

	hints := &openpgp.FileHints{
		IsBinary: true,
	}

	pgpWriter, err := openpgp.Encrypt(dst, publicKey, privateKey[0], hints, nil)
	if err != nil {
		return err
	}
	defer pgpWriter.Close()

	gzipWriter, err := gzip.NewWriterLevel(pgpWriter, 9)
	if err != nil {
		return err
	}
	defer gzipWriter.Close()

	if _, err := io.Copy(gzipWriter, src); err != nil {
		return err
	}

	gzipWriter.Flush()

	return nil
}

// DescryptFile decrypts a gpg file and validates the signature. The uncompressed (gzip) file is then written to the destination.
func DescryptFile(srcFile string, dstFile string, passphrase string, privateKeyFile, publicKeyFile string) error {
	privateKeyData, err := ioutil.ReadFile(privateKeyFile)
	if err != nil {
		return errors.ReadKeyError{Msg: fmt.Sprintf("Failed to read the private key: %s", err.Error())}
	}
	publicKeyData, err := ioutil.ReadFile(publicKeyFile)
	if err != nil {
		return errors.ReadKeyError{Msg: fmt.Sprintf("Failed to read the public key: %s", err.Error())}
	}

	src, err := os.Open(srcFile)
	if err != nil {
		return err
	}
	defer src.Close()

	dst, err := os.OpenFile(dstFile, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer dst.Close()

	return descrypt(src, dst, passphrase, privateKeyData, publicKeyData)
}

func descrypt(src io.Reader, dst io.Writer, passphrase string, privateKeyData []byte, publicKeyData []byte) error {
	privateKey, err := importProtectedKey(privateKeyData, passphrase)
	if err != nil {
		return err
	}

	msg, err := openpgp.ReadMessage(src, privateKey, nil, nil)
	if err != nil {
		return err
	}

	if err := checkSignature(msg, publicKeyData); err != nil {
		return err
	}

	gzipReader, err := gzip.NewReader(msg.UnverifiedBody)
	if err != nil {
		return err
	}
	defer gzipReader.Close()

	if _, err := io.Copy(dst, gzipReader); err != nil {
		return err
	}

	return nil
}
