/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"log"
	"os"
)

const configcontent = `# Your IANA id.
ianaID: 9999

# depositBaseDir - the directory where the depositfiles can be found
depositBaseDir: examples/deposits/rde

# runDir - the directory where the processed files will be located
runDir: deposit-run-rde

# compressAndEncrypt - if set to true, a hashfile of the depositfiles 
# will be created and the files will be compressed and encrypted.
# set this to false if you just want to do validation of your files.
compressAndEncrypt: true

# uploadFiles - if set to true, the processed files will 
# be uploaded to the escrow agent
uploadFiles: false

# multi - if set to true, all deposit files (except hash) are assumed
# to be part of a sequence of files. You need to add a sequence number
# at the end of the filename like "foobar_full_1.csv", "foobar_full_2.csv", ...
# Deposit files without a sequence number will not be processed when multi
# is active. Only the first file of the sequence should contain the header definitions.
multi: false

# useFileSystemCache - if set to true, the application will store all
# temporary data on the disk and delete it on completion. Activate this
# option if you run low on memory during execution.
useFileSystemCache: false

# gpg configuration parameters
gpg:
  # gpgPrivateKeyPath - path to your private key which is used
  # to sign the deposit files 
  gpgPrivateKeyPath: examples/keys/your.private.gpg.key

  # gpgPrivateKeyPass - password to decrypt your private key
  gpgPrivateKeyPass: test

  # gpgReceiverPubKeyPath - path to the public key of the escrow agent
  # which is used to encrypt the deposit
  gpgReceiverPubKeyPath: examples/keys/ote-escrow.denic-services.de.public.gpg.key

# sftp configuration parameters
sftp:
  # sshHostname - the hostname to upload the deposit to
  sshHostname: ote-escrow.denic-services.de
  
  # sshPort - the port to connect to
  sshPort: 22

  # sshUsername - the username for authenticating at the remote end
  sshUsername: username

  # sshPrivateKeyPath - the path to your private ssh key used for connecting
  sshPrivateKeyPath: examples/keys/your.private.sshkey

  # sshPrivateKeyPassword - the password to decrypt your private ssh key
  # comment line if key has no password
  sshPrivateKeyPassword: test123

  # sshHostPublicKeyPath - the path to the hostkey of the escrow agents endpoint
  # comment line if you want to accept any presented host key
  # sshHostPublicKeyPath: examples/keys/denic.hostkey
`

func createConfigFile() {
	filename := fmt.Sprintf("%s-%s.yaml", "config-rde-client-example", appversion)

	_, err := os.Stat(filename)
	if err != nil && !os.IsNotExist(err) {
		log.Fatalf("an error occured: %s\n", err.Error())
	} else {
		cfile, err := os.OpenFile(filename, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0644)
		if err != nil {
			log.Fatalf("Cannot write configfile example: %s\n", err.Error())
		}
		defer cfile.Close()
		cfile.WriteString(configcontent)
		fmt.Printf("Example config written to %s\n", filename)
	}
}
