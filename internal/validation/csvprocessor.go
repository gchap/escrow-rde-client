/*
 Copyright (C) 2018 DENIC eG

 This file is part of escrow-rde-client.

	escrow-rde-client is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	escrow-rde-client is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with escrow-rde-client.  If not, see <https://www.gnu.org/licenses/>.
*/

package validation

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"

	"gitlab.com/team-escrow/escrow-rde-client/internal/datastructures"
	"gitlab.com/team-escrow/escrow-rde-client/internal/errors"
)

type csvValidator interface {
	ReportFileBegin(filePath string)
	ValidateRow(values []string) error
	ReportDuplicateEntry(values []string)
}

type csvProcessor struct {
	options   Options
	cache     *datastructures.CsvCache
	validator csvValidator
}

func newCsvProcessor(headers []string, validator csvValidator, options Options) (*csvProcessor, error) {
	cache, err := datastructures.NewCsvCache(headers, options.UseFileSystemCache)
	if err != nil {
		return nil, err
	}

	return &csvProcessor{options, cache, validator}, nil
}

func (processor *csvProcessor) Validate(files []string, reporter Reporter) error {
	for i, filePath := range files {
		processor.validator.ReportFileBegin(filePath)

		file, err := os.Open(filePath)
		if err != nil {
			return errors.ValidateReadCsvFileError{Msg: err.Error()}
		}
		defer file.Close()

		fi, err := file.Stat()
		if err != nil {
			return errors.ValidateReadCsvFileError{Msg: err.Error()}
		}
		if uint64(fi.Size()) > processor.options.MaximumCsvSize {
			putError(reporter, errors.CsvTooLargeError{Msg: fmt.Sprintf("Input csv file '%v' is too large.", file.Name())})
		}

		lineCount := 0
		reader := csv.NewReader(file)
		if i == 0 {
			// first row in the first file contains the headers -> skip this row
			reader.Read()
			lineCount++
		}
		for {
			values, err := reader.Read()
			if err == io.EOF {
				// end of file reached, just stop the loop
				break
			} else if err != nil {
				switch err.(type) {
				case *csv.ParseError:
					parseErr, _ := err.(*csv.ParseError)
					if parseErr.Err == csv.ErrFieldCount {
						return errors.InvalidColumnCountError{Msg: fmt.Sprintf("Line %v: expected %v columns but got %v.", lineCount+1, reader.FieldsPerRecord, len(values))}
					}
					return errors.ValidateParseCsvFileError{Msg: parseErr.Error()}
				default:
					return err
				}
			}

			lineCount++

			err = processor.cache.Add(values)
			if err != nil {
				switch err.(type) {
				case errors.MultipleEntryError:
					processor.validator.ReportDuplicateEntry(values)
				default:
					return err
				}
			} else {
				if err := processor.validator.ValidateRow(values); err != nil {
					return err
				}
			}
		}

		// ICANN specification requires at max 1000000 lines per csv
		if lineCount > processor.options.MaximumCsvRows {
			putError(reporter, errors.TooManyRowsError{
				Msg: fmt.Sprintf("All csv-files must be splitted after %v rows, but found %v", processor.options.MaximumCsvRows, lineCount)})
		}
	}

	return nil
}

func (processor *csvProcessor) GetCache() *datastructures.CsvCache {
	return processor.cache
}
